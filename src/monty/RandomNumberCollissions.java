package monty;

import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import static java.time.Duration.between;
import static java.time.Instant.now;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Stream.iterate;

/**
 * This 'monty carlo' simulates: for an amount of 'people' assign a random number in
 * the range of 'masked(macRange)' return the amount of people that have a number that someone else also has ( collusion )
 *
 * In a nutshell: the distribution of people in groups having the same last mac digits.
 *
 * In reality the last 4 random hexadecimals are probably less random than this pseudorandom generator i use.
 *
 * with settings:
 * TOTAL_RUNS:100 000 000
 * MAC_RANGE: 0xFFFF
 * PEOPLE: 500
 * my machine gave this response:
 * {0=14833571, 2=28448248, 3=72815, 4=27062775, 5=137911, 6=17029365, 7=129558, 8=7972750, 9=80351, 10=2963739, 11=37078, 12=908679, 13=13620, 14=237200, 15=4106, 16=54007, 17=1005, 18=10619, 19=222, 20=1929, 21=57, 22=345, 23=8, 24=37, 25=1, 26=3, 28=1}
 * PT13M59.751596S
 */
public class RandomNumberCollissions {

    static final int TOTAL_RUNS = 100_000_000;
    static final int MAC_RANGE = 0xFFFF; // random numbers will be chosen in this range
    static final int PEOPLE = 500;  // amount of people that are given a random mac address per run

    /**
     * Run the simulation with above settings.
     */
    public static void main(String[] args) {

        Instant start = now();

        Map<Long, Long> countPerOcclusees = iterate(0, i -> i)
                .limit(TOTAL_RUNS)
                .parallel()
                .map(RandomNumberCollissions::simulate)
                .collect(groupingBy(l -> l, counting()));
        Instant end = now();

        System.out.println(countPerOcclusees);
        System.out.println(between(start, end));
    }

    /**
     * One single simulation
     * @param x Ignore, just so i can method-reference in the stream.
     * @return amount of people having at least one other person with the same random number.
     */
    static Long simulate(int x) {
        Map<Integer, Long> counts = ThreadLocalRandom.current()
                .ints(PEOPLE, 0, MAC_RANGE)
                .boxed()
                .collect(groupingBy(l -> l, counting()));

        return counts.values()
                .stream()
                .filter(i -> i > 1)
                .reduce(0L, Long::sum);
    }
}
